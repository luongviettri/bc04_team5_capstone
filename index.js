var headerEl = document.getElementById("header");
var classListItem = document.getElementById("myBody");
/**
 * hàm dùng để chỉnh màu header khi scroll chuột
 */
function changeHeaderColor() {
  var header = document.querySelector("#header");
  var navItems = document.querySelectorAll(".nav-link");
  // convert to array
  navItems = [...navItems];

  var brandBig = document.getElementById("brand");
  var brandSmall = document.getElementById("brand-small");

  var moon = document.querySelector(".fa-moon");
  if (document.documentElement.scrollTop > 80) {
    header.classList.add("set-height");

    for (var i = 0; i < navItems.length; i++) {
      if (navItems[i] !== navItems[0]) {
        navItems[i].classList.add("nav-item-color");
      } else {
        navItems[i].classList.add("first-nav-item-color");
      }
    }

    brandBig.classList.add("color-web");
    brandSmall.classList.add("color-web");
    moon.classList.add("color-web");
  } else {
    header.classList.remove("set-height");
    for (var i = 0; i < navItems.length; i++) {
      if (navItems[i] !== navItems[0]) {
        navItems[i].classList.remove("nav-item-color");
      } else {
        navItems[i].classList.remove("first-nav-item-color");
      }
    }
    brandBig.classList.remove("color-web");
    brandSmall.classList.remove("color-web");
    moon.classList.remove("color-web");
  }
}
//tạo hàm thay đổi giao diện
function changeTheme() {
  document.getElementById("switchButton").onclick = function () {
    document.getElementById("myBody").classList.toggle("dark");
  };
}
changeTheme();
function changeHeaderColorWhenClickMenu() {
  var header = document.querySelector("#header").classList;
  if (header.contains("header-click-color")) {
    header.remove("header-click-color");
  } else {
    header.add("header-click-color");
  }
}
/*  hết phần của tui rồi nghen */
